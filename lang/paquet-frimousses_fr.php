<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-frimousses
// Langue: fr
// Date: 09-10-2019 16:22:21
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// F
	'frimousses_description' => 'Remplace les codes ASCII pour les frimousses par des petites images.',
	'frimousses_slogan' => 'Mettre des smileys dans le texte',
);
?>