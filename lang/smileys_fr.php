<?php
  
$GLOBALS[$GLOBALS['idx_lang']] = array(
	':-)*'=> 'bisou',
	':-))'=> 'PTDR',
	':-)'=> 'sourire',
	'o:)'=> 'ange',
	'%-)'=> 'sidéré',
	';-)'=> 'clin d\'œil',
	':-(('=> 'triste',
	':\'-(' => 'pleure',
	':-O'=> 'colère',
	':-|'=> 'sans voix',
	':-/'=> 'perplexe',
	':-p'=> 'langue tirée',
	':-...'=> 'timide',
	':...'=> 'timide',
	':-x'=> 'muet',
	'B-)'=> 'lunettes de soleil',
	':-@'=> 'endormi',
	':-$'=> 'avide',
	':-!'=> 'indécis',
	'|-)'=> 'neutre',
);

?>